# circuitpython-gps-clock

## Name
CircuitPython GPS Clock is partially a clock and partially a R+D platform for the GPS module.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Visuals
(To be added later)

## Installation
(A list of libraries goes here)

## Usage
(Some examples go here)

## Support
Please open issues in the GitLab issue system at:

https://gitlab.com/SpringCitySolutionsLLC/circuitpython-gps-clock/-/issues

or contact via email at support@springcitysolutions.com

## Roadmap
Add most of the UI for individual satellite analysis, the data the cloud bistatic radar would be automatically processing in an IoT manner.

## Contributing
Please send a pull request for MIT licensed contributions.  Thanks!

## Authors and acknowledgment
Code written by Spring City Solutions LLC

Huge appreciation to Adafruit for the inspirational MIT licensed example code for the hardware.

## License
MIT license

## Project status
In-progress

