# code.py

# Copyright (c) 2022-2023 Spring City Solutions LLC and other contributors
# Licensed under the MIT license

# SPDX-FileCopyrightText: 2022 Spring City Solutions LLC
# SPDX-License-Identifier: MIT

# Originally this clock-alike project is/was a precursor R+D platform for
# a bistatic distributed "cloud" RADAR project, which is a very long story.
# This specific hardware stack does not have network connectivity but is
# adequate for playing around with the GPS module which would be used in
# production for the bistatic cloud RADAR.

# This is written in CircuitPython which is different from MicroPython
# https://docs.circuitpython.org/en/latest/README.html#differences-from-micropython

# Then MicroPython is different from regular CPython in the following ways
# https://docs.micropython.org/en/latest/genrst/index.html

# Intended to be run on this Adafruit Feather-ecosystem hardware stack
# Mini Color TFT FeatherWing Adafruit 3321
# GPS FeatherWing Adafruit 3133
# Feather M4 Express Adafruit 3857
# CircuitPython 7.3.3

import time
import board
import busio
import rtc
import time

# https://github.com/adafruit/Adafruit_CircuitPython_GPS
# https://docs.circuitpython.org/projects/gps/en/latest/
import adafruit_gps

# https://github.com/adafruit/Adafruit_CircuitPython_FeatherWing
# https://docs.circuitpython.org/projects/featherwing/en/latest/
from adafruit_featherwing import minitft_featherwing

# minitft_featherwing pulls in adafruit_st7735r
# https://github.com/adafruit/Adafruit_CircuitPython_ST7735R
# https://docs.circuitpython.org/projects/st7735r/en/latest/

# minitft_featherwing pulls in adafruit_seesaw
# https://github.com/adafruit/Adafruit_CircuitPython_seesaw
# https://docs.circuitpython.org/projects/seesaw/en/latest/

# minitft_featherwing pulls in adafruit_bus_device
# https://github.com/adafruit/Adafruit_CircuitPython_BusDevice
# https://docs.circuitpython.org/projects/busdevice/en/latest/

import terminalio
import displayio
from adafruit_display_text import label

# This flag will be set True after first valid GPS fix acquired
# No point in displaying GPS acquired data until after GPS signal
# is acquired at least one time.
atLeastOneGPSFix = False

# Default timezone, in hours of UTC offset, is CST
# This has to be manually adjusted WRT DST
tzoffset = -6

# Current Display Mode.  
# List of Modes:
# 1 HH:MM:SS in 24 hour format
# 2 Roman Numerals in 24 hour format
displayMode = 1

# This should numerically equal the highest value in the mode list above
displayModeMax = 2

# timeStringOld is the previous time display, its the previous value of timeString
timeStringOld = "LOL NO"

minitft = minitft_featherwing.MiniTFTFeatherWing()

backlight = True

display = minitft.display

# After the first valid GPS fix, the clock will display the RTC time
# which will continue to update even if GPS fix is lost for a short
# period of time
rtc = rtc.RTC()

# Timeout for serial has to be a little longer for GPS feathers
# TODO: Don't know why but everyone does it this way ... should I continue to do so?
uart = busio.UART(board.TX, board.RX, baudrate=9600, timeout=10)

gps = adafruit_gps.GPS(uart, debug=False)
gps = adafruit_gps.GPS(uart, debug=False)
# TODO: The bistatic radar would request the GSV and RMC sentence
gps.send_command(b"PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
gps.send_command(b"PMTK220,500")

waiting_group = displayio.Group()
text = "Waiting for GPS"
font = terminalio.FONT
color = 0xFFFFFF
hms_area = label.Label(font, text=text, color=color)
hms_area.x = 50
hms_area.y = 40
waiting_group.append(hms_area)
display.show(waiting_group)

gps.update()

last_print = time.monotonic()
while True:
    gps.update()

    current = time.monotonic()
    if current - last_print >= 0.1:
        last_print = current

        buttons = minitft.buttons

        # It is true that +14:00 offset from UTC exists as a timezone.
        if buttons.a:
            tzoffset +=1
            if tzoffset > 14:
                tzoffset = 14

        if buttons.b:
            tzoffset -= 1
            if tzoffset < -12:
                tzoffset = -12

        if buttons.select:
            if backlight == True:
                # Backlight Off
                backlight = False
                display.show(None)
                minitft.backlight = 1
            else:
                # Backlight On
                backlight = True
                minitft.backlight = 0

        if buttons.up:
            displayMode += 1
            if displayMode > displayModeMax:
                displayMode = displayModeMax

        if buttons.down:
            displayMode -= 1
            if displayMode < 1:
                displayMode = 1

        if gps.has_fix:
            atLeastOneGPSFix = True
            # Sometimes the GPS reported datetime will be invalid year 0, so skip that.
            # Set the internal RTC in local timezone using a GPS UTC timestamp
            # GPS C time struct converted to epoch seconds,
            # then add the local TZ offset in seconds (aka hours time 3600),
            # then convert back into a C time struct,
            # then set the internal RTC
            if gps.datetime.tm_year > 0:
                rtc.datetime = time.localtime((time.mktime(gps.datetime) + (tzoffset * 3600)))

        if atLeastOneGPSFix and backlight:
        
            # Psuedocode of a typical displayMode
            # Calculate a timeString based on what will be displayed.
            #   Trivially, the timeString itself will be displayed.
            # Compare the timeString to the timeStringOld
            # If the timeString is new aka != timeStringOld
            #   timeStringOld = timeString
            #   go to the effort to create and output a new display

            if displayMode == 1:
                # HH:MM:SS in 24 hour format
                timeString = "{displaymode:02d} {hour:02d}:{minute:02d}:{second:02d}".format(
                    displaymode=displayMode,
                    hour=rtc.datetime.tm_hour or 0,
                    minute=rtc.datetime.tm_min or 0,
                    second=rtc.datetime.tm_sec or 0,
                )
                if gps.has_fix:
                    # gps.satellites can unfortunately be TypeNone which crashes int() yet also not false
                    timeString += " {:02d}".format(int(gps.satellites or 0))
                if timeString != timeStringOld:
                    timeStringOld = timeString
                    clockGroup = displayio.Group()
                    font = terminalio.FONT
                    color = 0xFFFFFF
                    displaymode_area = label.Label(font, 
                        text="{displayMode:02d}".format(displayMode=displayMode), 
                        color=color)
                    displaymode_area.x = 0
                    displaymode_area.y = 4
                    clockGroup.append(displaymode_area)
                    satCount_area = label.Label(font, 
                        text="{satCount:02d}".format(satCount=int(gps.satellites or 0)), 
                        color=color)
                    satCount_area.x = display.width - 12
                    satCount_area.y = display.height - 8
                    clockGroup.append(satCount_area)
                    hms = "{hour:02d}:{minute:02d}:{second:02d}".format(
                        hour=rtc.datetime.tm_hour or 0,
                        minute=rtc.datetime.tm_min or 0,
                        second=rtc.datetime.tm_sec or 0,
                    )
                    hms_area = label.Label(font, text=hms, color=color, scale=3)
                    hms_area.x = int(display.width / 2) - int(len(hms)*6*3/2)
                    hms_area.y = int(display.height / 2)
                    clockGroup.append(hms_area)
                    display.show(clockGroup)

            if displayMode == 2:
                # Roman Numerals in 24 hour format
                timeString = "{displaymode:02d} {hour:02d}:{minute:02d}:{second:02d}".format(
                    displaymode=displayMode,
                    hour=rtc.datetime.tm_hour or 0,
                    minute=rtc.datetime.tm_min or 0,
                    second=rtc.datetime.tm_sec or 0,
                )
                if gps.has_fix:
                    # gps.satellites can unfortunately be TypeNone which crashes int() yet also not false
                    timeString += " {:02d}".format(int(gps.satellites or 0))
                if timeString != timeStringOld:
                    timeStringOld = timeString
                    clockGroup = displayio.Group()
                    font = terminalio.FONT
                    color = 0xFFFFFF
                    displaymode_area = label.Label(font, 
                        text="{displayMode:02d}".format(displayMode=displayMode), 
                        color=color)
                    displaymode_area.x = 0
                    displaymode_area.y = 4
                    clockGroup.append(displaymode_area)
                    satCount_area = label.Label(font, 
                        text="{satCount:02d}".format(satCount=int(gps.satellites or 0)), 
                        color=color)
                    satCount_area.x = display.width - 12
                    satCount_area.y = display.height - 8
                    clockGroup.append(satCount_area)
                    roman = "{hour:02d}:{minute:02d}".format(
                        hour=rtc.datetime.tm_hour or 0,
                        minute=rtc.datetime.tm_min or 0,
                    )
                    romanInteger = (rtc.datetime.tm_hour or 0) * 100
                    romanInteger += (rtc.datetime.tm_min or 0)
                    roman = ""
                    if romanInteger >= 1000:
                        roman += "M"
                        romanInteger -= 1000
                    if romanInteger >= 900:
                        roman += "CM"
                        romanInteger -= 900
                    if romanInteger >= 500:
                        roman += "D"
                        romanInteger -= 500
                    if romanInteger >= 400:
                        roman += "CD"
                        romanInteger -= 400
                    while romanInteger >= 100:
                        roman += "C"
                        romanInteger -= 100
                    if romanInteger >= 50:
                        roman += "L"
                        romanInteger -= 50
                    if romanInteger >= 40:
                        roman += "XL"
                        romanInteger -= 40
                    while romanInteger >= 10:
                        roman += "X"
                        romanInteger -= 10
                    if romanInteger >= 9:
                        roman += "IX"
                        romanInteger -= 9
                    if romanInteger >= 5:
                        roman += "V"
                        romanInteger -= 5
                    if romanInteger >= 4:
                        roman += "IV"
                        romanInteger -= 4
                    while romanInteger >= 1:
                        roman += "I"
                        romanInteger -= 1
                    # Worst case test scenario, I think?
                    # roman = "MDCCCCXXVIII"                                                                               
                    roman_area = label.Label(font, text=roman, color=color, scale=2)
                    roman_area.x = int(display.width / 2) - int(len(roman)*6*2/2)
                    roman_area.y = int(display.height / 2)
                    clockGroup.append(roman_area)
                    display.show(clockGroup)
